import { Table, TableContainer, TableHead, TableRow, TableCell, TableBody, Paper, Button, Container} from "@mui/material";
import { useEffect, useState } from "react";
import ModalInfo from "./modal-info";


function UserData() {
    const [data, setData] = useState([]);
    const [openModal, setOpenModal] = useState(false);
    const getData = async () => {
        const Fetch = await fetch('http://42.115.221.44:8080/crud-api/users/');
        const response = await Fetch.json();
        return response;
    }
    useEffect(() => {
        getData().then((data) => {
            console.log(data);
            setData(data);
        }).catch((error) => {
            console.log(error);
        })
    }, []);
    const Modify = (post) => {
        console.log(post.id);
    }
    const Delete = (post) => {
        console.log(post.id);
    }
    const onBtnAddUserClk = () => {
        setOpenModal(true);
    };
    return (
        <Container>
            <h3 align='center'>Danh sách người dùng đăng ký</h3>
            <Button variant="contained" color="primary" sx={{ marginY: 1 }} onClick={onBtnAddUserClk}>Thêm User</Button>
            <ModalInfo open={openModal} setOpen={setOpenModal}/>
            <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell align="center" sx={{ fontWeight: 'bold' }}>ID</TableCell>
                        <TableCell align="center" sx={{ fontWeight: 'bold' }}>First name</TableCell>
                        <TableCell align="center" sx={{ fontWeight: 'bold' }}>Last name</TableCell>
                        <TableCell align="center" sx={{ fontWeight: 'bold' }}>Country</TableCell>
                        <TableCell align="center" sx={{ fontWeight: 'bold' }}>Subject</TableCell>
                        <TableCell align="center" sx={{ fontWeight: 'bold' }}>Customer Type</TableCell>
                        <TableCell align="center" sx={{ fontWeight: 'bold' }}>Registry Status</TableCell>
                        <TableCell align="center" sx={{ fontWeight: 'bold' }}>Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.map((row, index) => (
                        <TableRow
                            key={index}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell component="th" scope="row">
                                {row.id}
                            </TableCell>
                            <TableCell align="left">{row.firstname}</TableCell>
                            <TableCell align="left">{row.lastname}</TableCell>
                            <TableCell align="center">{row.country}</TableCell>
                            <TableCell align="left">{row.subject}</TableCell>
                            <TableCell align="center">{row.customerType}</TableCell>
                            <TableCell align="center">{row.registerStatus}</TableCell>
                            <TableCell align="center"><Button variant="contained" color="success" onClick={()=>Modify(row)}>Sửa</Button>
                                                    <Button variant="contained" color="info" sx={{ marginLeft: 1 }} onClick={()=>Delete(row)}>Xóa</Button></TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
        </Container>
    )
}
export default UserData;