import { Modal, Box, Grid, Input, Select, MenuItem, Button } from "@mui/material";
import { useEffect, useState } from "react";
import SnackBar from "./snackbar";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function ModalInfo({ open, setOpen }) {
    const handleClose = () => setOpen(false);
    //Stack bar
    const [openStack, setOpenStack] = useState(false);
    //status
    const [status, setStatus] = useState(false);
    //input data
    const [firstname, setFirstName] = useState("");
    const [lastname, setLastName] = useState("");
    const [subject, setSubject] = useState("");
    const [country, setCountry] = useState("");
    //set data
    const firstNameChange=(event) => {
        setFirstName(event.target.value);
    }
    const lastNameChange=(event) => {
        setLastName(event.target.value);
    }
    const subjectChange=(event) => {
        setSubject(event.target.value);
    }
    const countryChange=(event) => {
        setCountry(event.target.value);
    }
    //validate
    const validateInputData = () => {
        if(firstname.trim()===""){
            alert("Input first name!");
            return false;
        }
        if(lastname.trim()===""){
            alert("Input last name!");
            return false;
        }
        if(subject.trim()===""){
            alert("Input Subject!");
            return false;
        }
        if(country.trim()===''){
            alert("Select Country!");
            return false;
        }
        return true;
    }
    //Tạo state đối tượng lưu thông tin user
    const [user, setUser] = useState({
        firstname:"",
        lastname:"",
        subject:"",
        country:""
    })
    //Call API
    const CallAPI = async(paramUrl, paramOption = {}) => {
        const response = await fetch(paramUrl,paramOption);
        const responseData = await response.json();
        return responseData;
    }
    const onBtnInsertUserClk = () => {
        if(validateInputData())
        {
            setUser({
                firstname:firstname,
                lastname: lastname,
                subject: subject,
                country: country
            });
            CallAPI('http://42.115.221.44:8080/crud-api/users/',{
            method: 'POST',
            body: JSON.stringify(user),
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            },
          }).then((data) => {
            console.log(data);
            setOpenStack(true);
            setStatus(true);
            setOpen(false);
            setFirstName("");
            setLastName("");
            setCountry("");
            setSubject("");
            console.log(user);
        })
        .catch((error) =>{
            console.log(error);
            setOpenStack(true);
            setStatus(false);
            console.log(user);
        });
        }
    }
    
    useEffect(() => {
        
    },[user,setOpen])
    return (
        <Grid>
            <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <Grid container spacing={2} sx={{ paddingBottom: 2 }}>
                    <Grid item xs={3} sx={{ paddingTop: 1 }}>
                        <label>First name</label>
                    </Grid>
                    <Grid item xs={9}>
                        <Input fullWidth onChange={firstNameChange}></Input>
                    </Grid>
                </Grid>
                <Grid container spacing={2} sx={{ paddingBottom: 2 }}>
                    <Grid item xs={3} sx={{ paddingTop: 1 }}>
                        <label>Last name</label>
                    </Grid>
                    <Grid item xs={9}>
                        <Input fullWidth onChange={lastNameChange}></Input>
                    </Grid>
                </Grid>
                <Grid container spacing={2} sx={{ paddingBottom: 2 }}>
                    <Grid item xs={3} sx={{ paddingTop: 1 }}>
                        <label>Subject</label>
                    </Grid>
                    <Grid item xs={9}>
                        <Input fullWidth onChange={subjectChange}></Input>
                    </Grid>
                </Grid>
                <Grid container spacing={2} sx={{ paddingBottom: 2 }}>
                    <Grid item xs={3} sx={{ paddingTop: 1 }}>
                        <label>Country</label>
                    </Grid>
                    <Grid item xs={9}>
                        <Select fullWidth defaultValue=''
                            onChange={countryChange}
                        >
                            <MenuItem value='VN'>Việt Nam</MenuItem>
                            <MenuItem value='USA'>USA</MenuItem>
                            <MenuItem value='AUS'>Australia</MenuItem>
                            <MenuItem value='CAN'>Canada</MenuItem>
                        </Select>
                    </Grid>
                </Grid>
                <Grid container spacing={2} sx={{ paddingBottom: 3 }}>
                    <Grid item xs={6} sx={{ paddingTop: 1, paddingLeft:2, textAlign:'center' }}>
                        <Button variant="contained" color="success" onClick={onBtnInsertUserClk}>Insert User</Button>
                    </Grid>
                    <Grid item xs={6} sx={{ paddingTop: 1, paddingLeft:3, textAlign:'center' }}>
                        <Button variant="contained" color="info">Cancel</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
        <SnackBar open={openStack} setOpen={setOpenStack} state= {status}/>
        </Grid>
    )
}
export default ModalInfo;