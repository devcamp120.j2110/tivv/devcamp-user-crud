import { Stack, Snackbar, Alert} from "@mui/material";

function SnackBar({open, setOpen, state}) {
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
        setOpen(false);
    }
    return(
        <Stack spacing={2} sx={{ width: '100%' }}>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        {state ? <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
          Thêm người dùng mới thành công!
        </Alert> : <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
          Thêm người dùng bị lỗi!
        </Alert>}
      </Snackbar>
    </Stack>
    )
}
export default SnackBar;